from django.contrib import admin
from .models import Student
from .models import Term
from .models import Score
# Register your models here.

@admin.register(Student)
class admin_student(admin.ModelAdmin):
    fieldsets= [
        ('Personal Informations', {"fields": ['student_ID', 'national_ID', 'name', 'last_Name', 'father', 'profile_pic']}),
        ('Contact', {'fields': ['home_phone', 'father_cell_phone', 'cell_phone', 'address']}),
        ('Extra Informations', {"fields": ['term', 'registered_on', 'extra_info']}),
    ]

    list_display = ('name', 'last_Name','student_ID', 'father', 'registered_on')
    search_fields = ('name', 'last_Name', 'national_ID', 'student_ID')
    readonly_fields = ('registered_on'),

@admin.register(Term)
class admin_term(admin.ModelAdmin):
    list_display = ('term',)
    search_fields = ('term',)


@admin.register(Score)
class admin_score(admin.ModelAdmin):
    ScoreList = ['class_activity', 'mid_term', 'attendence', 'writing', 'final']
    list_display = ScoreList
