from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class KarnameConfig(AppConfig):
    name = 'karname'   
    verbose_name = _('برنامه کارنامه')
