from django.db import models
from django.utils import timezone
from .validations import integer_validation
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class Student(models.Model):

    student_ID = models.SlugField(
        max_length=10,
        unique=True,
        validators=[integer_validation],
        verbose_name='دانش آموز ID')

    national_ID = models.CharField(
        max_length=2,
        null=True,
        unique=True,
        validators=[integer_validation],
        verbose_name='کد ملی')

    name = models.CharField(
        max_length=15,
        verbose_name='نام')

    last_Name = models.CharField(
        max_length=15,
        verbose_name='نام خانوادگی')

    father = models.CharField( 
        max_length=15,
        verbose_name='نام پدر')

    term = models.ManyToManyField('Term', related_name='terms')

    profile_pic = models.ImageField(
        upload_to='images',
        verbose_name='نمایه')


    registered_on = models.DateTimeField(
        default=timezone.now,
        verbose_name='تاریخ ثبت‌ نام')

    cell_phone = models.CharField(
        max_length=11,
        validators=[integer_validation]
        ,verbose_name='شماره تلفن دانش‌آموز')

    father_cell_phone = models.CharField(
        max_length=11,
        validators=[integer_validation],
        verbose_name='تلفن همراه پدر')

    home_phone = models.CharField(
        max_length=8,
        validators=[integer_validation],
        verbose_name='شماره منزل')

    address = models.TextField(verbose_name='آدرس')

    extra_info = models.TextField(blank=True, verbose_name='توضیحات اضافه')
    
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = _('دانش‌ آموز')
        verbose_name_plural = _('دانش‌ آموزان')

class Term(models.Model):
    term = models.CharField(max_length=10, unique=True, verbose_name='ترم')

    def __str__(self):
        return self.term

    class Meta:
        verbose_name = _('ترم')
        verbose_name_plural = _('ترم‌ها')


class Score(models.Model):
    class_activity = models.CharField(max_length=2, validators=[integer_validation], verbose_name='فعالیت کلاسی')
    mid_term = models.CharField(max_length=2, validators=[integer_validation], verbose_name='میان ترم')
    attendence = models.CharField(max_length=2, validators=[integer_validation], verbose_name='حضور در کلاس')
    writing = models.CharField(max_length=2, validators=[integer_validation], verbose_name='متن نویسی')
    final = models.CharField(max_length=2, validators=[integer_validation], verbose_name='پایان ترم')
    total = models.CharField(max_length=2, default=None)
    student = models.OneToOneField(Student, on_delete=models.CASCADE, verbose_name='دانش‌آموز')
    
    
    def __str__(self):
        return self.student
    

    def save(self, *args, **kwargs):
        self.total = self.class_activity + self.mid_term + self.attendence + self.writing + self.final 

        super(Score, self).save(*args, **kwargs)


    class Meta:
        verbose_name = _('نمره')
        verbose_name_plural = _('نمرات')
