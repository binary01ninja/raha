from django.core.exceptions import ValidationError

def integer_validation(value):
    try:
        int(value)
    except ValueError:
        raise ValidationError(('باید عدد صحیح وارد کنید'))


